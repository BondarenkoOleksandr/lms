from copy import copy


def get_query_params(request):
    query_params = request.GET.urlencode()

    if 'page' in request.GET:
        get_params = copy(request.GET)
        del get_params['page']
        query_params = get_params.urlencode()

    return {
        'query_params': query_params
    }
