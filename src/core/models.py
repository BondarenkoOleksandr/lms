import random

from core.validators import validate_email_for_prohibited_domain, validate_phone_number

from django.contrib.auth.models import User
from django.db import models

from faker import Faker


class Person(models.Model):
    class Meta:
        abstract = True

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    age = models.IntegerField(null=False, default=42)

    email = models.EmailField(max_length=64, validators=[validate_email_for_prohibited_domain])
    phone_number = models.CharField(null=False,
                                    max_length=20,
                                    validators=[validate_phone_number],
                                    default="+38(098)000-0000")

    inn = models.PositiveIntegerField(unique=True, null=True)

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age}'

    @classmethod
    def _generate(cls):
        faker = Faker()
        person_object = cls(
            first_name=faker.first_name(),
            last_name=faker.last_name(),
            age=random.randint(18, 140)
        )
        person_object.save()
        return person_object

    @classmethod
    def generate(cls, count):
        for _ in range(count):
            cls._generate()

    def full_name(self):
        return f'{self.first_name} {self.last_name}'


class Logger(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    path = models.CharField(max_length=128)
    create_date = models.DateTimeField(auto_now_add=True)
    execution_time = models.FloatField()
    query_params = models.CharField(max_length=64, null=True)
    info = models.CharField(max_length=128, null=True)
