import re

from django.core.exceptions import ValidationError


def validate_email_for_prohibited_domain(value):
    BLACKLIST = ['zzz.com']  # noqa

    valid = True

    _, _, email_domain = value.partition('@')

    if email_domain in BLACKLIST:
        raise ValidationError('Prohibited domain')

    return valid


def validate_phone_number(value):
    SHORT_LENGTH = 13  # noqa

    valid = True

    pattern = "(\(\d{3}\)|\+\d{2}\(\d{3}\))\d{3}\-\d{4}"  # noqa

    if not re.match(pattern, value):
        raise ValidationError('Phone number is not correct')

    if len(value) == SHORT_LENGTH:
        value = '+38' + value

    return valid
