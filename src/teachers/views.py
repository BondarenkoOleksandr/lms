from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from teachers.forms import TeacherBaseForm, TeacherCreateForm, TeacherFilter, TeacherUpdateForm
from teachers.models import Teacher


class TeacherListView(LoginRequiredMixin, ListView):
    paginate_by = 5
    model = Teacher
    template_name = 'teachers-list.html'
    context_object_name = 'teachers'

    def get_filter(self):
        return TeacherFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()

        return context


class TeacherCreateView(CreateView):
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teachers-create.html'
    success_url = reverse_lazy('teachers:list')


class TeacherUpdateView(UpdateView):
    model = Teacher
    form_class = TeacherUpdateForm
    template_name = 'teachers-update.html'
    success_url = reverse_lazy('teachers:list')
    pk_url_kwarg = 'teacher_id'


class TeacherDeleteView(DeleteView):
    model = Teacher
    form_class = TeacherBaseForm
    template_name = 'teachers-delete.html'
    success_url = reverse_lazy('teachers:list')
    pk_url_kwarg = 'teacher_id'
