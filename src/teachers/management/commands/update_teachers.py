from django.core.management.base import BaseCommand

from faker import Faker

from teachers.models import Teacher


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        teachers = Teacher.objects.all()
        faker = Faker()

        for teacher in teachers:
            teacher.update(phone_number=faker.phone_number(),
                           birth_date=faker.date(),
                           email=faker.email())

        self.stdout.write("Fields updated")
