import random

from django.core.management.base import BaseCommand

from faker import Faker

from teachers.models import Teacher


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help='Count of random teachers')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        faker = Faker()

        for _ in range(count):
            teacher_object = Teacher(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                age=random.randint(20, 105),
                about=faker.text(),
                phone_number=faker.phone_number(),
                birth_date=faker.date(),
                email=faker.email()
            )

            teacher_object.save()

        self.stdout.write(f"{count} random teachers created")
