def format_list(records):
    response = ''
    for entry in records:
        link = f'<a href="/teachers/update/{entry.id}">UPDATE</a>'
        response += '<br>' + link + ' ' + str(entry)
    return response if response else '(Empty result)'
