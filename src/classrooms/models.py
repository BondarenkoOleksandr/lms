import random

from django.db import models


# Create your models here.
from faker import Faker


class Classroom(models.Model):
    name = models.CharField(max_length=200, null=False)
    room_number = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.name}, {self.room_number}'

    @classmethod
    def generate_classrooms(cls, count):
        faker = Faker()
        for _ in range(count):
            classroom_object = cls(
                name=faker.company(),
                room_number=random.randint(1, 200)
            )
            classroom_object.save()
