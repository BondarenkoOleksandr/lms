from classrooms.forms import ClassroomDeleteForm, ClassroomFilter, ClassroomUpdateForm
from classrooms.models import Classroom

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView, ListView, UpdateView


class ClassroomListView(ListView):
    model = Classroom
    template_name = 'classrooms-list.html'

    def get_filter(self):
        return ClassroomFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        return context


class ClassroomUpdateView(LoginRequiredMixin, UpdateView):
    model = Classroom
    form_class = ClassroomUpdateForm
    success_url = reverse_lazy('classrooms:list')
    template_name = 'classrooms-update.html'
    pk_url_kwarg = 'classroom_id'


class ClassroomDeleteView(LoginRequiredMixin, DeleteView):
    model = Classroom
    form_class = ClassroomDeleteForm
    template_name = 'classrooms-delete.html'
    success_url = reverse_lazy('classrooms:list')
    pk_url_kwarg = 'classroom_id'
