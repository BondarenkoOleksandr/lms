from classrooms.views import ClassroomDeleteView, ClassroomListView, ClassroomUpdateView

from django.urls import path


app_name = "classrooms"

urlpatterns = [
    path('', ClassroomListView.as_view(), name="list"),
    path('edit/<int:classroom_id>/', ClassroomUpdateView.as_view(), name="update"),
    path('delete/<int:classroom_id>/', ClassroomDeleteView.as_view(), name="delete")
]
