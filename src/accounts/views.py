from accounts.forms import AccountProfileUpdateForm, AccountRegistrationForm, AccountUpdateForm

from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.views.generic.edit import ProcessFormView
from django.shortcuts import render  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User registered successfully')
        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'
    success_url = reverse_lazy('index')

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'User {self.request.user} logged in')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'
    success_url = reverse_lazy('index')

    def get_redirect_url(self):
        return reverse('index')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.success(self.request, f'User {self.request.user} logged out')
        return super().dispatch(request, *args, **kwargs)


class AccountUpdateView(ProcessFormView):
    # model = User
    # template_name = 'profile.html'
    # success_url = reverse_lazy('index')
    # form_class = AccountUpdateForm
    #
    # def get_object(self, queryset=None):
    #     return self.request.user
    #
    # def form_valid(self, form):
    #     result = super().form_valid(form)
    #     messages.success(self.request, f'Profile user {self.request.user} updated')
    #     return result
    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(data=request.POST,
                                      instance=user)
        profile_form = AccountProfileUpdateForm(data=request.POST,
                                                files=request.FILES,
                                                instance=profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form
            }
        )


class AccountPasswordChangeView(PasswordChangeView):
    template_name = 'password-change.html'
    success_url = reverse_lazy('index')
