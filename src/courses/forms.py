from courses.models import Course

from django.forms import ModelForm, MultipleChoiceField

import django_filters

from teachers.models import Teacher


class CourseFilter(django_filters.FilterSet):
    class Meta:
        model = Course
        fields = {
            'name': ['exact', 'icontains']
        }


class CourseBaseForm(ModelForm):
    class Meta:
        model = Course
        fields = '__all__'


class CourseUpdateForm(CourseBaseForm):
    class Meta(CourseBaseForm.Meta):
        exclude = ['teachers']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        teachers = [(tc.id, tc.full_name()) for tc in Teacher.objects.all()]
        self.fields['teachers_field'] = MultipleChoiceField(choices=teachers)


class CourseCreateForm(CourseBaseForm):
    pass
