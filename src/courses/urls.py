from courses.views import CourseDeleteView, CourseListView, CourseUpdateView

from django.urls import path

app_name = "courses"

urlpatterns = [
    path('', CourseListView.as_view(), name="list"),
    path('edit/<int:course_id>/', CourseUpdateView.as_view(), name='update'),
    path('delete/<int:course_id>/', CourseDeleteView.as_view(), name="delete")
]
