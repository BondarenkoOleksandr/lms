import datetime

from django.db import models


class Group(models.Model):
    name = models.CharField(max_length=100, null=False)
    number_of_students = models.IntegerField(null=True)
    date_of_creation = models.DateField(null=True, default=datetime.date.today)
    headman = models.OneToOneField(
        to='students.Student',
        on_delete=models.SET_NULL,
        null=True,
        related_name='headed_group'
    )

    classrooms = models.ManyToManyField(
        to="classrooms.Classroom",
        related_name="groups"
    )

    def __str__(self):
        return f'{self.name}, {self.number_of_students}, {self.date_of_creation}'

    def get_teachers(self):
        return ', '.join(
            teacher.full_name()
            for teacher in self.teachers.all()
        )
