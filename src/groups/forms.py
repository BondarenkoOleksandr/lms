from django.forms import ChoiceField, ModelForm

import django_filters

from groups.models import Group


class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Group
        fields = {
            'name': ['exact', 'icontains'],
        }


class GroupBaseForm(ModelForm):
    class Meta:
        model = Group
        fields = '__all__'


class GroupCreateForm(GroupBaseForm):
    pass


class GroupUpdateForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        exclude = ['start_date', 'headman']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        students = [(st.id, st.full_name()) for st in self.instance.students.all()]
        self.fields['headman_field'] = ChoiceField(choices=students)
        self.fields['headman_field'].initial = [self.instance.headman.id]

    def save(self, commit=True):
        headman_id = self.cleaned_data['headman_field']
        self.instance.headman = self.instance.students.get(id=headman_id)
        return super().save(commit=True)
