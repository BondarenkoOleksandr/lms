from django.contrib import admin  # noqa

# Register your models here.
from groups.models import Group

from students.models import Student

from teachers.models import Teacher


class StudentTable(admin.TabularInline):
    model = Student
    fields = ['first_name', 'last_name', 'email']
    readonly_fields = fields
    show_change_link = True
    max_num = 10


class TeachersTable(admin.TabularInline):
    model = Teacher
    fields = ['first_name', 'last_name', 'email']
    readonly_fields = fields
    show_change_link = True
    max_num = 10


class GroupAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['name']
    inlines = [StudentTable, TeachersTable]
    list_select_related = ['headman']


admin.site.register(Group, GroupAdmin)
