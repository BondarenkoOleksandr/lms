from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentBaseForm, StudentCreateForm, StudentFilter, StudentUpdateForm
from students.models import Student


class StudentListView(LoginRequiredMixin, ListView):
    paginate_by = 5
    model = Student
    template_name = 'students-list.html'
    context_object_name = 'students'

    def get_filter(self):
        return StudentFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs.select_related('group', 'headed_group')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()

        return context


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-create.html'


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentUpdateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-update.html'
    pk_url_kwarg = 'student_id'


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Student
    form_class = StudentBaseForm
    success_url = reverse_lazy('students:list')
    template_name = 'students-delete.html'
    pk_url_kwarg = 'student_id'
